var timer = null;

function show_result(result_task_id) {
    $.ajax({
        url: '/show_result/' + result_task_id,
        type: 'GET',
        success: function (msg) {
            response_task = msg;

            // ***********************************************
            // The answer format (response_task) must be as below:
            // ***********************************************
            // {'first_class': 'C2',
            // 'first_class_percentage': '48.81%',
            // 'second_class': 'C1',
            // 'second_class_percentage': '31.99%'
            // }

            document.getElementById("prediction").innerHTML = '<p>Your text is being queued and will be processed after previously submitted texts. This could take some time depending on the number of users.</p>';
            // document.getElementById("test_result").innerHTML = response_task.status;
            if (response_task.status == 'SUCCESS') {
                clearInterval(timer);
                // document.getElementById("prediction").innerHTML = "The answer for the text '" + response_task.content['text'] + "' is " + response_task.content['prediction'];
                // document.getElementById("prediction").innerHTML = "The prediction for the text is: <br \>" + response_task.content['prediction'];
                document.getElementById("prediction_title").innerHTML = "Prediction of text level";
                document.getElementById("prediction").innerHTML = '';
                document.getElementById("first_class").innerHTML = response_task.content['first_class_percentage'] + " level " + response_task.content['first_class'];
                document.getElementById("second_class").innerHTML = response_task.content['second_class_percentage'] + " level " + response_task.content['second_class'];
                document.getElementById("send").disabled = false;

                if (response_task.content['first_class'] == 'B2') {
                    document.getElementById("level_image").src = "../static/B2Level.png";
                    // document.getElementById("first_class").style.color = "pink";

                    return;
                }
                if (response_task.content['first_class'] == 'B1') {
                    document.getElementById("level_image").src = "../static/B1Level.png";
                    return;
                }

                if (response_task.content['first_class'] == 'A2') {
                    document.getElementById("level_image").src = "../static/A2Level.png";
                    // document.getElementById("first_class").style.color = "green";

                    return;
                }

                if (response_task.content['first_class'] == 'A1') {
                    document.getElementById("level_image").src = "../static/A1Level.png";
                    return;
                }

                if (response_task.content['first_class'] == 'C1') {
                    document.getElementById("level_image").src = "../static/C1Level.png";
                    return;
                }

                if (response_task.content['first_class'] == 'C2') {
                    document.getElementById("level_image").src = "../static/C2Level.png";
                    return;
                }
                // document.getElementById("prediction").style.color = "green";


                return;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            document.getElementById("prediction").innerHTML = 'An error has occurred: it was not possible to process your request.'
            clearInterval(timer);
        }
    });
}

function change_prediction_result(text) {
    var output_text = "Your predicted probabilities for each level were: <br> "
    for (var key in text) {
        if (text.hasOwnProperty(key)) {
            output_text = output_text.concat(key + ": " + text[key] + " ")
        }
    }
    // output_text = output_text.concat(text)
    // output_text = output_text.concat(" }")
    document.getElementById("prediction").innerHTML = output_text
}

function show_feedback_to_user(result_task_id) {
    document.getElementById("prediction_title").innerHTML = 'Please, wait';

    document.getElementById("prediction").innerHTML = '<p>Your text is being queued and will be processed after previously submitted texts. This could take some time depending on the number of users.</p>';
    document.getElementById("first_class").innerHTML = '';
    document.getElementById("second_class").innerHTML = '';
    document.getElementById("prediction").style.color = "red";
    document.getElementById("level_image").src = "../static/all_levels.png";
    document.getElementById("send").disabled = true;
    timer = setInterval(function () {
            show_result(result_task_id);
        },
        10000)
}

function make_request(text, method = "post") {
    $.ajax({
        url: '/prediction',
        type: 'POST',
        data: JSON.stringify({
            message: text
        }),
        contentType: 'application/json',
        processData: false,
        success: function (msg) {
            task_created = msg;
            show_feedback_to_user(task_created.result_task_id);

        }
        // async: false
    });
    // change_prediction_result(prediction);

}

function mock_make_request(essay_text) {
    var prediction = "{\"A1\": \"0.2\", \"A2\": \"0.1\", \"B1\": \"0.5\", \"B2\": \"0.0\", \"C1\": \"0.1\" , \"C2\": \"0.1\"}";
    change_prediction_result(prediction);
}

window.onload = function () {
    document.getElementById("send").onclick = function () {
        essay_text = document.getElementById("essayArea").value;
        make_request(essay_text);
        // mock_make_request(essay_text);
    }
    // var element = document.getElementById("div1");
}
